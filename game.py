from random import randint
name = input("Hi! What is your name?")
for guesses in range(5):
    month = randint(1,12)
    year = randint(1924, 2004)
    print("Guess " + str(guesses + 1) + ": " + str(name) + " were you born in " + str(month) + " / " + str(year))
    guess_correct = input("yes or no?")
    if guess_correct == "yes":
        print("I knew it!")
        exit()
    elif guess_correct == "no" and guesses > 4:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")