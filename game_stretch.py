from random import randint
name = input("Hi! What is your name?")
guess_number = 1
months = [
    "January",
    "Febuary",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
]
month = randint(1,12)
month_max = 12
month_min = 1
year = randint(1924, 2004)
year_max = 2004
year_min = 1924
day = randint(1, 30)
day_max = 30
day_min = 1
##day is incorrect at this point as it is only between 1 and 30 and does not take into account the month-  will work on this if I get time

for guesses in range(5):
    print("Guess " + str(guess_number) + ": " + str(name) + " were you born in " + months[month - 1] + " " + str(day) + ", " + str(year))
    guess_number = guess_number + 1
    guess_correct = input("yes or no?")
    if guess_correct == "yes":
        print("I knew it!")
        break
    elif guess_correct != "yes" and guess_number > 5:
        print("I have other things to do. Good bye.")
    elif guess_correct == "later":
        ##we want to be later than the date provided from the previous randoms
        print("Drat! Lemme try again!")
        year = randint(year, year_max)
        month = randint(month, month_max)
        day = randint(day, day_max)
    else:
        #This is the "earlier answer"
        year = randint(year_min, year)
        month = randint(month_min, month)
        day = randint(day_min, day)
        print("Drat! Lemme try again!")